/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxswing;

/**
 *
 * @author werapan
 */
public class Player {
    private char name;
    private int win;
    private int lose;
    private int draw;

    public Player(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + '}';
    }

    public int getWin() {
        return this.win;
    }

    public int getLose() {
        return this.lose;
    }

    public int getDraw() {
        return this.draw;
    }
    
    public void win() {
        this.win++;
    }
    
    public void lose() {
        this.lose++;
    }
    
    public void draw() {
        this.draw++;
    }
    
}
